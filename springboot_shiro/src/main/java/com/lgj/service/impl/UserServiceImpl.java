package com.lgj.service.impl;

import com.lgj.mapper.UserMapper;
import com.lgj.pojo.Power;
import com.lgj.pojo.Role;
import com.lgj.pojo.User;
import com.lgj.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：MrL
 * 时间：2020-05-10
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findOne(String uname) {
        return userMapper.selectOne(uname);
    }

    @Override
    public List<Role> findAllRolesById(int id) {
        return userMapper.selectAllRoles(id);
    }

    @Override
    public List<Power> findAllPowersById(int id) {
        return userMapper.selectAllPowers(id);
    }

    @Override
    public List<Power> findAllPowers() {
        return userMapper.selectAll();
    }
}
