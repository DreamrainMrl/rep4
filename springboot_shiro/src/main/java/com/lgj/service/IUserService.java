package com.lgj.service;

import com.lgj.pojo.Power;
import com.lgj.pojo.Role;
import com.lgj.pojo.User;

import java.util.List;

/**
 * @author ：MrL
 * 时间：2020-05-10
 */
public interface IUserService {

    /**
     * 根据用户名查询 用户
     * @param uname
     * @return
     */
    User findOne(String uname);

    /**
     *  查询认证用户对应的所有的 角色
     * @param id
     * @return
     */
    List<Role> findAllRolesById(int id);

    /**
     * 查询认证用户所以的 权限
     * @param id
     * @return
     */
    List<Power> findAllPowersById(int id);

    /**
     * 查询所有权限 对应的 url
     * 进行过滤
     * @return
     */
    List<Power> findAllPowers();
}
