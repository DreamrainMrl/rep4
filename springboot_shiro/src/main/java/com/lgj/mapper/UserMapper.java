package com.lgj.mapper;

import com.lgj.pojo.Power;
import com.lgj.pojo.Role;
import com.lgj.pojo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author ：MrL
 * 时间：2020-05-10
 */
public interface UserMapper {

    @Select("select * from user where uname = #{param1}")
    User selectOne(String uname);

    @Select("select r.rid,r.rname from user u join user_role ur on u.ID = ur.uid join role r on ur.rid = r.rid where u.id = #{param1}")
    List<Role> selectAllRoles(int id);

    @Select("select p.pid, p.pname\n" +
            "from role r join role_power rp on r.rid = rp.rid join power p on rp.pid = p.pid\n" +
            "where r.rid in (select r.rid from user u join user_role ur on u.ID = ur.uid join role r on ur.rid = r.rid\n" +
            "where u.id = #{param1}) ")
    List<Power> selectAllPowers(int id);

    @Select("select p.pname, f.url from power p join power_func pf on p.pid = pf.pid join func f on pf.fid = f.fid")
    List<Power> selectAll();
}
