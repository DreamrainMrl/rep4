package com.lgj.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：MrL
 * 时间：2020-05-13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Power {

    private Integer pid;

    private String pname;

    private String url;
}
