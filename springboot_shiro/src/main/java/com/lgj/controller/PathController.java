package com.lgj.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;

/**
 * @author ：MrL
 * 时间：2020-05-11
 */
@Controller
public class PathController {

    @RequestMapping("/update")
    @ResponseBody
    public String update() {
        return "update";
    }

    @RequestMapping("/insert")
    @ResponseBody
    public String insert() {
        return "insert";
    }

    @RequestMapping("/delete")
    @ResponseBody
    @RequiresPermissions("sys:delete")
    public String delete() {
        return "delete";
    }

    @RequestMapping("/select")
    @ResponseBody
    public String select() {
        return "select";
    }

    @RequestMapping("/isPermission")
    @ResponseBody
    @RequiresPermissions("isPermission")
    public String isPermission() {
        return "isPermission";
    }

    @RequestMapping("/show")
    @ResponseBody
    public String show(HttpSession session) {
        // 获取session中所有的键值
        Enumeration<String> enumeration = session.getAttributeNames();

        // 遍历Enumeration
        while (enumeration.hasMoreElements()) {
            // 获取session 键值
            String key = enumeration.nextElement().toString();
            // 根据键值key获取session的value值
            Object value = session.getAttribute(key);
            // 打印结果
            System.out.println("key: " + key + " value : " + value);
        }
        return "session";
    }
}
