package com.lgj.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author ：MrL
 * 时间：2020-05-11
 */
@Controller
public class UserController {

    @RequestMapping("/{path}")
    public String getUrl(@PathVariable String path) {

        System.out.println("restful");
        return path;
    }

    @RequestMapping("/doLogin")
    public String login(String uname, String pwd, @RequestParam(defaultValue = "false") boolean rm) {
        Subject subject = SecurityUtils.getSubject();
        try {
            // 进行 令牌认证 -- 调往 MyRealm
            subject.login(new UsernamePasswordToken(uname, pwd, rm));
            return "main";
        } catch (AuthenticationException e) {
            System.out.println("认证失败");
            return "login";
        }
    }

}
