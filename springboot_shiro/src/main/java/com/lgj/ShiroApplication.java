package com.lgj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot程序的入口类，即 启动类
 * @author ：MrL
 * 时间：2020-05-11
 */
@SpringBootApplication
@MapperScan("com.lgj.mapper")
public class ShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroApplication.class, args);
    }
}
