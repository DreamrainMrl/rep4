package com.lgj.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.lgj.pojo.Power;
import com.lgj.realm.MyRealm;
import com.lgj.service.IUserService;
import net.sf.ehcache.CacheManager;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.io.ResourceUtils;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;

/**
 * @author ：MrL
 * 时间：2020-05-10
 */
@Configuration
public class ShiroConfig {

    @Autowired
    private MyRealm myRealm;

    @Autowired
    private IUserService userService;

    @Bean
    public DefaultWebSecurityManager defaultWebSecurityManager() {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        // 创建一个新的 哈希凭证匹配器
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        // 设置凭证匹配器 的算法名称
        matcher.setHashAlgorithmName("md5");
        // 设置凭证匹配器 迭代次数
        matcher.setHashIterations(3);
        // 将凭证匹配器 注入到 自定义的realm中
        myRealm.setCredentialsMatcher(matcher);

        // 将自定义 realm 注入到 安全管理器中
        defaultWebSecurityManager.setRealm(myRealm);

        // 将自定义 rememberme设置到 securityManager中
        defaultWebSecurityManager.setRememberMeManager(rememberMeManager());

        // 将自定义的缓存管理器 设置到 securityManager中
        defaultWebSecurityManager.setCacheManager(ehCacheManager());

        return defaultWebSecurityManager;
    }


    /**
     * 记住我 操作
     *
     * @return
     */
    public CookieRememberMeManager rememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        // 设置自定义Cookie
        cookieRememberMeManager.setCookie(rememberMeCookie());
        // 设置密码秘钥 setCipherKey(byte[] cipherKey)
        cookieRememberMeManager.setCipherKey("1234567890123456".getBytes());
        return cookieRememberMeManager;
    }

    /**
     * 自定义 Cookies
     *
     * @return
     */
    private SimpleCookie rememberMeCookie() {
        SimpleCookie cookie = new SimpleCookie("MrL");
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        // cookie 有效期 1个月
        cookie.setMaxAge(30 * 24 * 60 * 60);
        return cookie;
    }

    /**
     * 定义 shiro拦截器链
     * anon表示 不用认证也可以访问
     * authc 表示 必须认证
     * user 表示 有用户就行，即用户认证完成后 所有请求都行
     *
     * @return
     */
    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition definition = new DefaultShiroFilterChainDefinition();

        definition.addPathDefinition("/doLogin", "anon");
        definition.addPathDefinition("/login", "anon");

        // 退出登录 内置 logout 进行过滤 退出的同时会删除 cookie 和 session
        definition.addPathDefinition("/loginOut", "logout");

        // 添加 权限url 的过滤
        List<Power> allPowers = userService.findAllPowers();
        for (Power allPower : allPowers) {
            String pname = allPower.getPname();
            String url = allPower.getUrl();
            definition.addPathDefinition(url, "perms["+ pname +"]");
        }

        // 确保每一个请求中都包含用户
        definition.addPathDefinition("/**", "user");
        return definition;
    }

    /**
     * 负责解析 thymeleaf 中 shiro:xxx 相关属性
     * @return
     */
    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }

    public EhCacheManager ehCacheManager() {
        EhCacheManager ehCacheManager = new EhCacheManager();
        InputStream is = null;
        try {
            // 加载缓存配置文件
            is = ResourceUtils.getInputStreamForPath("classpath:ehcache/ehcache.xml");
            CacheManager cacheManager = new CacheManager(is);
            ehCacheManager.setCacheManager(cacheManager);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ehCacheManager;
    }
}

