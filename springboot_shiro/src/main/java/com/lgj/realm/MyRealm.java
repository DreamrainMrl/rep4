package com.lgj.realm;

import com.lgj.pojo.Power;
import com.lgj.pojo.Role;
import com.lgj.pojo.User;
import com.lgj.service.IUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author ：MrL
 * 时间：2020-05-10
 */
@Component
public class MyRealm extends AuthorizingRealm {

    @Autowired
    private IUserService userService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        System.out.println("进入了鉴权方法");

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();

        User user = (User) principalCollection.getPrimaryPrincipal();

        // 查询认证用户对应所以的角色
        List<Role> roles = userService.findAllRolesById(user.getId());
        // 将查询到的角色添加到 鉴权信息中
        Set<String> roleSet = new HashSet<>();
        for (Role role : roles) {
            roleSet.add(role.getRname());
        }
        authorizationInfo.addRoles(roleSet);

        // 查询认证用户对应所有的权限
        List<Power> powers = userService.findAllPowersById(user.getId());
        // 将 查询的权限 添加到 鉴权信息中
        Set<String> powerSet = new HashSet<>();
        for (Power power : powers) {
            powerSet.add(power.getPname());
        }
        authorizationInfo.addStringPermissions(powerSet);
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 从令牌中获取 用户名
        String uname = authenticationToken.getPrincipal().toString();
        // 通过用户名 在数据库中查询 用户
        User user = userService.findOne(uname);
        if (user != null) {
            // 进行密码校验 -- 此处 为 加盐值校验
            // 此处跳转到  凭证匹配器 -- 即 ShiroConfig
            SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPwd(), ByteSource.Util.bytes(user.getSalt()), "nana");
            return info;
        }
        return null;
    }
}
